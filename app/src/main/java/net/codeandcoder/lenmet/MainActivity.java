package net.codeandcoder.lenmet;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;


public class MainActivity extends ActionBarActivity implements SensorEventListener, View.OnClickListener {

    private SensorManager sensorManager;
    private Sensor accelerometer;

    private float length = 0;
    private long lastCheck = 0;
    private boolean scanning = false;

    private TextView text;
    private Button scan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorManager.registerListener(this, accelerometer , SensorManager.SENSOR_DELAY_FASTEST);

        text = (TextView) findViewById(R.id.length);
        scan = (Button) findViewById(R.id.scan);
        scan.setOnClickListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (scanning) {
            Sensor mySensor = event.sensor;

            if (mySensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
                float zAcceleration = event.values[2];
                long timeIncrement = System.currentTimeMillis() - lastCheck;
                float velocityIncrement = zAcceleration / timeIncrement;
                float positionIncrement = velocityIncrement / timeIncrement;
                length += positionIncrement;
                lastCheck = System.currentTimeMillis();
                String formattedLength = new DecimalFormat("#0.00").format(length) + " m";
                text.setText(formattedLength);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onClick(View v) {
        if (!scanning) {
            length = 0;
            lastCheck = System.currentTimeMillis();
            scanning = true;
            scan.setText(getResources().getString(R.string.stop));
        } else {
            scanning = false;
            scan.setText(getResources().getString(R.string.scan));
        }
    }
}
